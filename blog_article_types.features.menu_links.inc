<?php
/**
 * @file
 * blog_article_types.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function blog_article_types_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_current-events:current-events
  $menu_links['main-menu_current-events:current-events'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'current-events',
    'router_path' => 'current-events',
    'link_title' => 'Current Events',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_current-events:current-events',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_featured-articles:featured
  $menu_links['main-menu_featured-articles:featured'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'featured',
    'router_path' => 'featured',
    'link_title' => 'Featured Articles',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_featured-articles:featured',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_health--wellness-blog:health-wellness-blog
  $menu_links['main-menu_health--wellness-blog:health-wellness-blog'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'health-wellness-blog',
    'router_path' => 'health-wellness-blog',
    'link_title' => 'Health & Wellness Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_health--wellness-blog:health-wellness-blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_news-blog:news-blog
  $menu_links['main-menu_news-blog:news-blog'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'news-blog',
    'router_path' => 'news-blog',
    'link_title' => 'News Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_news-blog:news-blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_sports-blog:sports-blog
  $menu_links['main-menu_sports-blog:sports-blog'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'sports-blog',
    'router_path' => 'sports-blog',
    'link_title' => 'Sports Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_sports-blog:sports-blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_tech-blog:tech-blog
  $menu_links['main-menu_tech-blog:tech-blog'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'tech-blog',
    'router_path' => 'tech-blog',
    'link_title' => 'Tech Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_tech-blog:tech-blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Current Events');
  t('Featured Articles');
  t('Health & Wellness Blog');
  t('News Blog');
  t('Sports Blog');
  t('Tech Blog');


  return $menu_links;
}
