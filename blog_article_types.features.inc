<?php
/**
 * @file
 * blog_article_types.features.inc
 */

/**
 * Implements hook_views_api().
 */
function blog_article_types_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function blog_article_types_node_info() {
  $items = array(
    'current_events_blog' => array(
      'name' => t('Current Events Blog'),
      'base' => 'node_content',
      'description' => t('Add a new current events blog.'),
      'has_title' => '1',
      'title_label' => t('Blog Title'),
      'help' => '',
    ),
    'featured_articles_blog' => array(
      'name' => t('Featured Articles Blog'),
      'base' => 'node_content',
      'description' => t('Add a new featured articles blog.'),
      'has_title' => '1',
      'title_label' => t('Blog Title'),
      'help' => '',
    ),
    'health_and_wellness_blog' => array(
      'name' => t('Health and Wellness Blog'),
      'base' => 'node_content',
      'description' => t('Add a new health and wellness blog.'),
      'has_title' => '1',
      'title_label' => t('Blog Title'),
      'help' => '',
    ),
    'news_article_blog' => array(
      'name' => t('News Article Blog'),
      'base' => 'node_content',
      'description' => t('Add a news article blog.'),
      'has_title' => '1',
      'title_label' => t('Blog Title'),
      'help' => '',
    ),
    'sports_article_blog' => array(
      'name' => t('Sports Article Blog'),
      'base' => 'node_content',
      'description' => t('Add a new sports article blog.'),
      'has_title' => '1',
      'title_label' => t('Blog Title'),
      'help' => '',
    ),
    'tech_article_blog' => array(
      'name' => t('Tech Article Blog'),
      'base' => 'node_content',
      'description' => t('Add a new tech article blog.'),
      'has_title' => '1',
      'title_label' => t('Blog Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
